# Pijany Chińczyk / Drinking Game #

It is a board game for Android platform. It was made using Unity Engine (5.3.2). Project has to be opened in Unity Editor in order to run it or build it.

[Game is available for download in Google Play Store](https://play.google.com/store/apps/details?id=com.WidlyWGnoju.PijanyChinczyk)

Supported languages: Polish, English, Russian

# Game features: #

* Playing with unlimited number of friends
* Taking picture to use for a pawn
* Throwing 3D dice using physics engine
* Two game modes and three boards

## Main problems that I had to solve ##

* User Interface - scale, position of elements and actions on buttons (MessageBox.cs, MessagePanel.cs, Popup.cs, GameSettings.cs and Overseer.cs)
* Saving and loading data (Overseer.cs)
* Camera and pictures handling (CameraPhoto.cs)
* Translation (Lang.cs, TranslatedText.cs and Languages.xml)
* Dice behaviour (Dice.cs)
* Board fields implementation (everything in Assets/Scripts/Fields folder)