﻿using UnityEngine;
using UnityEngine.UI;

public class Toggle : MonoBehaviour
{
    public GameObject toggleObject;
    public GameObject parentObject;

    private RectTransform rectTransform;

    void Start ()
    {
        if (toggleObject)
            rectTransform = toggleObject.GetComponent<RectTransform> ();
    }

    public void ToggleObject ()
    {
        toggleObject.SetActive (!toggleObject.activeSelf);
    }

    public void ParentTo ()
    {
        toggleObject.transform.SetParent (parentObject.transform);
        rectTransform.anchoredPosition = new Vector2 (0, 0);
    }

    public void ParentTo (string gameObjectTag)
    {
        GameObject parent = GameObject.FindWithTag (gameObjectTag);
        toggleObject.transform.SetParent (parent.transform);
        rectTransform.anchoredPosition = new Vector2 (0, 0);
    }
}
