﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MessagePanel : MonoBehaviour 
{
    public Button acceptButton;
    public Button declineButton;
    public Image playerImage;
    public float opacity = 0.941f;
    public Text message;
    public Text playerName;

    private RectTransform rect;
    private Text buttonText;
    private Image image;

    void Awake ()
    {
        rect = GetComponent<RectTransform> ();
        buttonText = acceptButton.GetComponentInChildren<Text> ();

        Transform imageTransform = gameObject.transform.Find ("Image");
        if (imageTransform != null)
            image = imageTransform.GetComponent<Image> ();
    }

    void OnDisable ()
    {
        ResetPanel ();
    }

    public void AddAction (UnityAction action)
    {
        acceptButton.onClick.AddListener (action);
    }

    public void ResetPanel ()
    {
        // clear player photo
        if (image != null)
        {
            Color clear = image.color;
            clear.a = 0;
            image.color = clear;
        }

        if (playerName != null)
            playerName.text = "";

        // remove listeners from confirm button
        acceptButton.onClick.RemoveAllListeners ();

        if (declineButton != null)
            declineButton.onClick.RemoveAllListeners ();
    }

    public void ShowMessage (string msg)
    {
        message.text = msg;
        transform.SetParent (Overseer.mainCanvas.transform);
        rect.anchoredPosition = Vector2.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = new Vector3 (1, 1, 1);
    }

    public void ShowMessage (string msg, string name, Color color, Sprite picture)
    {
        message.text = msg;
        color.a = opacity;
        GetComponent<Image> ().color = color;

        if (playerName != null)
            playerName.text = name;
        
        if (picture.name != "Blank")
        {
            int margin = (picture.texture.width / 2) - (picture.texture.height / 2);
            Sprite sprite = Sprite.Create (picture.texture, new Rect (margin, 0, picture.texture.width - (margin * 2), picture.texture.height), new Vector2 (0.5f, 0.5f));
            playerImage.sprite = sprite;
            Color imageColor = playerImage.color;
            imageColor.a = 1;
            playerImage.color = imageColor;
        }
        else
        {
            if (image != null)
            {
                Color clear = image.color;
                clear.a = 0;
                image.color = clear;
            }
        }
    }

    public void ShowMessage (string msg, string name, Color color, Sprite picture, string buttonText)
    {
        message.text = msg;
        this.buttonText.text = buttonText;
        color.a = opacity;
        GetComponent<Image> ().color = color;

        if (playerName != null)
            playerName.text = name;

        if (picture.name != "Blank")
        {
            int margin = (picture.texture.width / 2) - (picture.texture.height / 2);
            Sprite sprite = Sprite.Create (picture.texture, new Rect (margin, 0, picture.texture.width - (margin * 2), picture.texture.height), new Vector2 (0.5f, 0.5f));
            playerImage.sprite = sprite;
            Color imageColor = playerImage.color;
            imageColor.a = 1;
            playerImage.color = imageColor;
        }
        else
        {
            if (image != null)
            {
                Color clear = image.color;
                clear.a = 0;
                image.color = clear;
            }
        }
    }

    public void ShowConfirmMessage (string msg)
    {
        message.text = msg;
        transform.SetParent (Overseer.mainCanvas.transform);
        rect.anchoredPosition = Vector2.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = new Vector3 (1, 1, 1);
    }
}