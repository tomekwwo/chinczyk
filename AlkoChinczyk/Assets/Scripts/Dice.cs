﻿using System.Collections;
using UnityEngine;

public class Dice : MonoBehaviour 
{
    public float gravity = 10;
    public float force = 10;
    public float torque = 15;
    public Transform[] sides;
    public float minThrowLength = 100;

    Rigidbody rigid;
    bool thrown = false;
    bool scoreGet = false;
    Vector2 throwVector;
    Vector2 startPos = Vector2.zero;
    int waitframe = 0;
    Vector3 diceStartPos;
    float timer = 0;
    bool beingMoved = false;

    void Awake ()
    {
        rigid = GetComponent<Rigidbody> ();
        rigid.isKinematic = true;
        startPos = new Vector2 ();
        diceStartPos = transform.position;
        enabled = false;
    }

    void Update ()
    {
        if (!thrown && !beingMoved)
            Throw ();
    }

    void FixedUpdate ()
    {
        if (thrown)
        {
            // follow dice with camera
            Camera.main.GetComponent<CameraController> ().FollowTarget (transform, 5);

            // add gravity force
            rigid.AddForce (Vector3.forward * gravity);
            
            // get result when dice stops spinning
            if (rigid.angularVelocity.magnitude < 0.005f && waitframe == 1 && !scoreGet)
            {
                CheckScore ();
                startPos = Vector2.zero;
            }
            waitframe = 1;

            //reset dice if its stuck 8 seconds after throw
            timer += Time.deltaTime;
            if (timer > 8 && scoreGet == false)
            {
                Overseer.instance.ShowPopup (Lang.instance.getString ("RepeatThrow"), 1);
                ResetDice ();
            }
        }
    }

    void Throw ()
    {
        if (Input.GetMouseButtonDown (0))
        {
            startPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp (0) && startPos != Vector2.zero)
        {
            Vector2 throwVector = (Vector2) Input.mousePosition - startPos;
                
            // check if vector length is not too small
            if (throwVector.magnitude >= minThrowLength)
            {
                rigid.AddForce (throwVector * force);
                rigid.AddTorque (throwVector * torque);
                thrown = true;
            }
            throwVector = Vector2.zero;
            startPos = Vector2.zero;
        }
    }

    void CheckScore ()
    {
        float minHeight = 0;
        int score = 0;
        for (int i = 0; i < sides.Length; i++)
        {
            if (minHeight > sides[i].position.z)
            {
                minHeight = sides[i].position.z;
                score = i + 1;
            }
        }

        GameController.instance.MoveActivePlayer (score);
        StartCoroutine (DeactivateDice ());
        scoreGet = true;
        thrown = false;
    }

    void OnEnable ()
    {
        ResetDice ();
    }

    void ResetDice ()
    {
        thrown = false;
        scoreGet = false;
        waitframe = 0;
        timer = 0;
        StartCoroutine (ActivateDice ());
    }

    public IEnumerator ActivateDice ()
    {
        yield return new WaitWhile (isMoving);

        StartCoroutine (Camera.main.GetComponent<CameraController> ().GoTo (Vector3.zero, 5));
        beingMoved = true;
        rigid.isKinematic = true;
        Vector3 newPos = new Vector3 (0, 0, diceStartPos.z);
        while (Vector3.Distance (transform.position, newPos) > 1)
        {
            transform.position = Vector3.Lerp (transform.position, newPos, Time.deltaTime * 5);
            yield return null;
        }
        rigid.isKinematic = false;
        beingMoved = false;
    }

    public IEnumerator DeactivateDice ()
    {
        yield return new WaitWhile (isMoving);

        beingMoved = true;
        rigid.isKinematic = true;
        while (Vector3.Distance (transform.position, diceStartPos) > 1)
        {
            transform.position = Vector3.Lerp (transform.position, diceStartPos, Time.deltaTime * 3);
            yield return null;
        }
        enabled = false;
        beingMoved = false;
    }

    public bool isMoving ()
    {
        return beingMoved;
    }
}