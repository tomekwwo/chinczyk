﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

public class Ranking : MonoBehaviour 
{
    public RectTransform content;
    public GameObject rankPrefab;

    private Player[] players;
    private Text[] ranks;

    void Start ()
    {
        Init ();
    }

    void Init ()
    {
        players = GameController.instance.players;
        ranks = new Text[players.Length];
        float spawnHeight = -50;

        // add all players to ranking
        for (int i = 0; i < players.Length; i++)
        {
            content.sizeDelta = new Vector2 (0, content.sizeDelta.y + 50);
            GameObject rank = Instantiate (rankPrefab);
            rank.transform.SetParent (content.transform);
            RectTransform spawnPosition = rank.GetComponent<RectTransform> ();
            spawnPosition.localScale = Vector3.one;
            spawnPosition.sizeDelta = new Vector3 (300, 50);
            spawnPosition.anchoredPosition = new Vector2 (0, spawnHeight);
            spawnHeight -= 50;
            ranks[i] = rank.GetComponent<Text> ();
            ranks[i].text = "" + (i + 1) + ". " + players[i].playerName + " - " + players[i].field.number;
            ranks[i].color = players[i].Color;
        }

        if (players.Length > 10)
            GetComponentInChildren<ScrollRect> ().vertical = true;

        Refresh (); 
    }

    public void Refresh ()
    {
        Dictionary<int, int> positions = new Dictionary<int, int> ();

        for (int i = 0; i < players.Length; i++)
        {
            positions.Add (i, players[i].field.number);
        }

        var items = from pair in positions orderby pair.Value descending select pair;

        int index = 0;
        foreach (KeyValuePair<int, int> pair in items)
        {
            SetRank (index, players[pair.Key]);
            index++;
        }
    }

    private void SetRank (int index, Player player)
    {
        if (player.finished)
        {
            ranks[index].text = "" + (index + 1) + ". " + player.playerName;
        }
        else
            ranks[index].text = "" + (index + 1) + ". " + player.playerName + " - " + player.field.number;

        ranks[index].color = player.Color;
    }
}