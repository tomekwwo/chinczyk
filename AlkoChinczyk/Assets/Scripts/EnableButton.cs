﻿using UnityEngine;
using UnityEngine.UI;

public class EnableButton : MonoBehaviour
{
    public Button button;

    public void Enable (bool b)
    {
        button.interactable = b;
    }

    public void Toggle ()
    {
        button.interactable = !button.interactable;
    }
}