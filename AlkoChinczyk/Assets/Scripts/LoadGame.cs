﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour 
{
    public static void Customize ()
    {
        SceneManager.LoadScene ("CustomizeGame");
    }

    public static void Menu ()
    {
        Overseer.instance.Ads.ShowAd ();
        Overseer.SaveOptions();
        SceneManager.LoadScene ("CustomizeGame");
    }

    public static void LoadScene (string name, bool showAd)
    {
        if (showAd)
            Overseer.instance.Ads.ShowAd ();

        Overseer.SaveOptions();
        SceneManager.LoadScene (name);
    }

    public static int GetActiveScene ()
    {
        return SceneManager.GetActiveScene ().buildIndex;
    }

    public static string GetActiveSceneName ()
    {
        return SceneManager.GetActiveScene ().name;
    }
}