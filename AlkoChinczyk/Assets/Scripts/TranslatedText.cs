﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class TranslatedText : MonoBehaviour
{
    public string phraseName;

    void Awake ()
    {
        SetText ();
    }

    void OnEnable ()
    {
        SetText ();
    }

    public void SetText ()
    {
        GetComponent<Text> ().text = Lang.instance.getString (phraseName);
    }
}
