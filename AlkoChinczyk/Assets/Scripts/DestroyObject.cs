﻿using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public GameObject obj;

    private bool removePlayer = false;

    void Start ()
    {
        if (obj.tag == "PlayerSettings")
            removePlayer = true;
    }

    public void DestroySelected ()
    {
        if (obj != null)
        {
            obj.SetActive (false);
            Destroy (obj);

            if (removePlayer)
            {
                GameSettings.instance.RemovePlayer ();
            }
        }
        else
            Debug.Log (name + " doesn't have object to destroy!");
    }
}
