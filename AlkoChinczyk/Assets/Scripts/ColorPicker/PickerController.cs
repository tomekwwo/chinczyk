﻿using UnityEngine;
using UnityEngine.UI;

public class PickerController : MonoBehaviour
{
    public float TextureSize => textureSize;

    private bool autosizeOnInit = true;
    [SerializeField] private Image targetImage;
    [SerializeField] private RectTransform pointer;
    [SerializeField] private ColorPicker picker;
    [SerializeField] private Slider hueSlider;
    [SerializeField] private float hueSliderThickness;
    [SerializeField] private Image chosenColorImage;
    [SerializeField] private float textureSize = 100;

    private Image pointerImage;

    public void Init(Color initColor, Image image)
    {
        pointerImage = pointer.GetComponent<Image>();
        
        if (autosizeOnInit)
        {
            // set proper size
            var size = (transform as RectTransform).sizeDelta;
            (picker.transform as RectTransform).sizeDelta = size;
            (hueSlider.transform as RectTransform).sizeDelta = new Vector2(size.x, hueSliderThickness);
        }
        
        // initiate picker
        targetImage = image;
        Color.RGBToHSV(initColor, out float h, out float s, out float v);
        hueSlider.value = h;
        picker.SetColor(s, v);
    }

    public void ColorChanged(Color color, Vector2 position)
    {
        targetImage.color = color;
        chosenColorImage.color = color;
        pointer.anchoredPosition = position;
        
        // update pointer color
        Color.RGBToHSV(color, out float h, out float s, out float v);
        if (s < 0.5f && v > 0.5f)
        {
            pointerImage.color = Color.black;
        }
        else
        {
            pointerImage.color = Color.white;
        }
    }
}
