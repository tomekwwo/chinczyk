﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ColorPicker : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private PickerController controller;

    private Vector2 MousePosition => rect.InverseTransformPoint(Input.mousePosition);
    
    private Image image;
    private RectTransform rect;
    private Texture2D tex;
    private float hue = 0;

    private bool pointerOver;
    private bool mouseDown;
    private Vector2 lastPosition = Vector2.zero;

    private void Awake()
    {
        rect = transform as RectTransform;
        image = GetComponent<Image>();
        GenerateGradient();
    }

    public void GenerateGradient()
    {
        Vector2 size = new Vector2(controller.TextureSize, controller.TextureSize);
        
        tex = new Texture2D((int)size.x, (int)size.y, DefaultFormat.LDR, TextureCreationFlags.None);
        float step = 1f / tex.width;
        float saturation = 0;
        for (int x = 0; x < tex.width; x++)
        {
            float brightness = 0;
            for (int y = 0; y < tex.height; y++)    
            {
                Color targetColor = Color.HSVToRGB(hue, saturation, brightness);
                tex.SetPixel(x, y, targetColor);
                brightness += step;
            }

            saturation += step;
        }
        tex.Apply();

        image.sprite = Sprite.Create(tex, new Rect(Vector2.zero, size), Vector2.zero);
    }

    private void Update()
    {
        UpdateColor();
    }

    private void UpdateColor(bool force = false)
    {
        if (mouseDown || force)
        {
            Vector2 positionOnPicker = MousePosition;
            if (force)
            {
                positionOnPicker = lastPosition;
            }
            ClampCursorPosition(ref positionOnPicker);
            var texPos = GetPositionOnTexture(positionOnPicker);
            Color newColor = tex.GetPixel(texPos.x, texPos.y);
            controller.ColorChanged(newColor, positionOnPicker);
            lastPosition = positionOnPicker;
        }
    }

    private void ClampCursorPosition(ref Vector2 position)
    {
        Vector2 pickerPosition = rect.anchoredPosition;
        position.x = Mathf.Clamp(position.x, pickerPosition.x, pickerPosition.x + rect.sizeDelta.x - 1);
        position.y = Mathf.Clamp(position.y, pickerPosition.y, pickerPosition.y + rect.sizeDelta.y - 1);
    }

    private Vector2Int GetPositionOnTexture(Vector2 cursorPosition)
    {
        var size = rect.sizeDelta;
        float x = cursorPosition.x / size.x * controller.TextureSize;
        float y = cursorPosition.y / size.y * controller.TextureSize;
        return new Vector2Int(Mathf.FloorToInt(x), Mathf.FloorToInt(y));
    }

    public void SetHue(float value)
    {
        hue = value;
        GenerateGradient();
        UpdateColor(true);
    }

    public void SetColor(float s, float v)
    {
        var size = rect.sizeDelta;
        lastPosition = new Vector2(s * size.x, v * size.y);
        UpdateColor(true);
    }

    #region Pointer events

    public void OnPointerDown(PointerEventData eventData)
    {
        if (pointerOver)
        {
            mouseDown = true;   
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        mouseDown = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        pointerOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pointerOver = false;
    }

    #endregion
}
