﻿using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HuePicker : MonoBehaviour
{
    [SerializeField] private Image hueImage;
    
    private RectTransform rect;
    private Texture2D hueTex;

    private void Awake()
    {
        rect = transform as RectTransform;
        GenerateHue();
    }

    private void GenerateHue()
    {
        Vector2 size = rect.sizeDelta;
        hueTex = new Texture2D((int)size.x, (int)size.y, DefaultFormat.LDR, TextureCreationFlags.None);
        float hue = 0;
        float step = 1f / hueTex.width;
        Color targetColor = Color.HSVToRGB(hue, 1, 1);
        
        for (int x = 0; x < hueTex.width; x++)
        {
            for (int y = 0; y < hueTex.height; y++)    
            {
                hueTex.SetPixel(x, y, targetColor);
            }

            hue = Mathf.Clamp01(hue + step);
            targetColor = Color.HSVToRGB(hue, 1, 1);
        }
        hueTex.Apply();

        hueImage.sprite = Sprite.Create(hueTex, new Rect(Vector2.zero, size), Vector2.zero);
    }    
}
