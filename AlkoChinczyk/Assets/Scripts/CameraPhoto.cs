﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class CameraPhoto : MonoBehaviour
{
    public Image feed;
    public Image render;
    public Player player;
    public bool forceRun = false;

    WebCamTexture cameraFeed;
    WebCamDevice[] devices;
    int activeDevice = 0;
    Vector2 symmetry = Vector2.zero;

    void Start ()
    {
        transform.SetParent (Overseer.mainCanvas.transform);
        RectTransform rect = GetComponent<RectTransform> ();
        rect.anchoredPosition = new Vector2 (0, 0);
        rect.sizeDelta = new Vector2 (0, 0);
        rect.localScale = new Vector3 (1, 1, 1);
    }

    void Update ()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            if (devices[activeDevice].isFrontFacing)
            {
                feed.transform.rotation = Quaternion.Euler (0, 180, 180);
                symmetry = new Vector2 (1, 0);
            }
            else
            {
                feed.transform.rotation = Quaternion.Euler (0, 0, 180);
                symmetry = new Vector2 (1, 1);
            }
        }
        else if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            if (devices[activeDevice].isFrontFacing)
            {
                feed.transform.rotation = Quaternion.Euler (0, 180, 0);
                symmetry = new Vector2 (0, 1);
            }
            else
            {
                feed.transform.rotation = Quaternion.Euler (0, 0, 0);
                symmetry = Vector2.zero;
            }
        }
    }

    void OnEnable ()
    {
        cameraFeed = new WebCamTexture ();
        devices = WebCamTexture.devices;
        if (devices.Length > 0 || forceRun)
        {
            feed.material.mainTexture = cameraFeed;
            for (int i = 0; i < devices.Length; i++)
            {
                if (devices[i].isFrontFacing)
                {
                    cameraFeed.deviceName = devices[i].name;
                    activeDevice = i;
                    break;
                }
            }
            
            cameraFeed.Play ();
        }
        else
        {
            Overseer.instance.ShowPopup (Lang.instance.getString ("NoCamera"));
            gameObject.SetActive (false);
        }
    }

    void OnDisable ()
    {
        if (cameraFeed.isPlaying)
        {
            cameraFeed.Stop ();
            cameraFeed = null;
            feed.material.mainTexture = null;
        }
    }

    public void ChangeCamera ()
    {
        activeDevice += 1;

        if (activeDevice >= devices.Length)
        {
            activeDevice = 0;
        }
        
        cameraFeed.Stop ();
        cameraFeed.deviceName = devices[activeDevice].name;
        cameraFeed.Play ();
    }

    public void TakePicture ()
    {
        StartCoroutine (TakePhoto ());
    }

    private IEnumerator TakePhoto ()
    {
        yield return new WaitForEndOfFrame ();

        Texture2D photo = new Texture2D (cameraFeed.width, cameraFeed.height);
        photo.SetPixels (cameraFeed.GetPixels ());

        // modify photo
        photo = DoSymmetry (photo);
        photo = CutCircle (photo, 0);

        photo.Apply ();

        // cut picture into square
        Sprite sprite = Sprite.Create (photo, new Rect (0, 0, photo.width, photo.height), new Vector2 (0.5f, 0.5f));
        player.photo = sprite;
        Color visible = render.color;
        visible.a = 1;
        render.color = visible;
        render.sprite = sprite;
        gameObject.SetActive (false);
    }

    private Texture2D CutCircle (Texture2D photo, int radius)
    {
        int r = Mathf.Min (photo.width, photo.height) / 2;

        if (radius > 0 && radius < r)
            r = radius;

        // cut photo into smallest square
        Texture2D outTex = new Texture2D (2 * r, 2 * r);
        int xMargin = Mathf.Abs ((2 * r - photo.width) / 2);
        int yMargin = Mathf.Abs ((2 * r - photo.height) / 2);
        
        for (int i = xMargin; i < photo.width - xMargin; i++)
        {
            for (int j = yMargin; j < photo.height - yMargin; j++)
            {
                outTex.SetPixel (i - xMargin, j - yMargin, photo.GetPixel (i, j));
            }
        }

        // create circle
        Vector2 middle = new Vector2 (outTex.width / 2, outTex.height / 2);
        for (int i = 0; i < outTex.width; i++)
        {
            for (int j = 0; j < outTex.height; j++)
            {
                Vector2 pos = new Vector2 (i, j);
                if (Vector2.Distance (pos, middle) > r)
                {
                    outTex.SetPixel (i, j, Color.clear);
                }
            }
        }

        return outTex;
    }

    private Texture2D DoSymmetry (Texture2D photo)
    {
        if (symmetry != Vector2.zero)
        {
            Texture2D symmetric = new Texture2D (photo.width, photo.height);
            for (int i = 0; i < photo.width; i++)
            {
                for (int j = 0; j < photo.height; j++)
                {
                    if (symmetry == new Vector2 (1, 1))
                        symmetric.SetPixel (i, j, photo.GetPixel (photo.width - i, photo.height - j));
                    else if (symmetry == new Vector2 (0, 1))
                        symmetric.SetPixel (i, j, photo.GetPixel (photo.width - i, j));
                    else if (symmetry == new Vector2 (1, 0))
                        symmetric.SetPixel (i, j, photo.GetPixel (i, photo.height - j));
                }
            }
            return symmetric;
        }
        else
            return photo;
    }

    public void LoadPhotoFromFile ()
    {
#if UNITY_EDITOR
        string[] filters = { "Image files", "png,jpg,jpeg" };
        Debug.Log ("File panel opening...");
        string file = EditorUtility.OpenFilePanelWithFilters ("Find picture", "", filters);
        byte[] pictureData = Overseer.LoadFile (file);

        // get picture data encoded to PNG
        Texture2D tex = new Texture2D (512, 512);
        tex.LoadImage (pictureData);
        pictureData = tex.EncodeToPNG ();
        tex.LoadImage (pictureData);

        // modify picture
        tex = CutCircle (tex, 256);
        tex.Apply ();

        // set player photo
        Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
        sprite.name = "picture";
        player.photo = sprite;
        Color visible = render.color;
        visible.a = 1;
        render.color = visible;
        render.sprite = sprite;
        gameObject.SetActive (false);
#endif
    }
}