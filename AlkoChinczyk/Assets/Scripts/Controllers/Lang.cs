﻿using UnityEngine;
using System.Collections;
using System.Xml;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Language
{
    AutoDetect, Polish, English, Russian
}

public class Lang : MonoBehaviour
{
    public TextAsset dictionary;
    public Language languageSet = Language.AutoDetect;

    public static Lang instance;

    private Hashtable Strings;
    private XmlDocument xml;

    void Awake ()
    {
        if (instance == null)
            instance = this;
        else
            Destroy (gameObject);
        if (instance != this) return;
        
        xml = new XmlDocument ();
        xml.LoadXml (dictionary.text);

        if (languageSet == Language.AutoDetect)
            languageSet = setLanguage (Application.systemLanguage.ToString ());
        else
            languageSet = setLanguage (languageSet.ToString ());
        
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        
        SetLanguageButtons ();
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (instance != this) return;
        
        SetLanguageButtons ();
    }

    void SetLanguageButtons ()
    {
        GameObject[] buttons = GameObject.FindGameObjectsWithTag ("LanguageButton");
        if (buttons.Length > 0)
        {
            foreach (GameObject button in buttons)
            {
                Button b = button.GetComponent<Button> ();
                b.onClick.AddListener (() => { ChangeLanguage (b.gameObject.name); });
            }
        }
    }

    public void ChangeLanguage (string language)
    {
        languageSet = setLanguage (language);

        TranslatedText[] texts = FindObjectsOfType<TranslatedText> ();
        foreach (TranslatedText text in texts)
        {
            text.SetText ();
        }
        Field[] fields = FindObjectsOfType<Field> ();
        foreach (Field field in fields)
        {
            field.SetText ();
        }
    }
    
    public Language setLanguage (string language)
    {
        Strings = new Hashtable ();
        if (xml.DocumentElement[language] != null)
        {
            XmlElement element = xml.DocumentElement[language];
            IEnumerator elemEnum = element.GetEnumerator ();
            
            while (elemEnum.MoveNext ())
            {
                XmlElement xmlItem = (XmlElement) elemEnum.Current;
                Strings.Add (xmlItem.GetAttribute ("name"), xmlItem.InnerText);
            }

            if (language == "Polish")
                return Language.Polish;
            else if (language == "English")
                return Language.English;
            else if (language == "Russian")
                return Language.Russian;
        }
        else
        {
            Debug.Log ("The specified language does not exist: " + language);
            return setLanguage ("English");
        }
        return Language.English;
    }
    
    public string getString (string name)
    {
        if (!Strings.ContainsKey (name))
        {
            Debug.LogError ("The specified string does not exist: " + name);

            return "ERROR";
        }
	
		return (string) Strings[name];
	}

}