﻿using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
    public float force = 400;
    public float mouseMultiplier = 8;
    public float touchMoveMultiplier = 4;
    public float minCameraSize = 30;
    public float maxCameraSize = 70;
    public float scrollZoomMultiplier = 1;
    public float pinchZoomMultiplier = 1;
    public float maxCameraSpeed = 400;

    private Rigidbody rigid;
    private Vector2 lastMousePos;
    private bool isEnabled = true;
    private float distance;

    void Start ()
    {
        rigid = GetComponent<Rigidbody> ();
        distance = transform.position.z;
    }

    void FixedUpdate ()
    {
        if (isEnabled && !MyInput.UIhit && MyInput.cursorHolding)
        {
            if (Input.touchSupported)
                TouchMovement ();
            else
                MoveWithMouse ();
        }
        else if (Application.platform != RuntimePlatform.Android && isEnabled)
        {
            MoveWithKeyboard ();
        }
    }
    
    void LateUpdate ()
    {
        if (Application.platform == RuntimePlatform.Android && isEnabled)
        {
            TouchZoom ();
        }
        else if (Application.platform != RuntimePlatform.Android && isEnabled)
        {
            ZoomWithMouseWheel ();
        }
    }

    public void EnableControls (bool enable)
    {
        isEnabled = enable;
    }

    public void FollowTarget (Transform target, float speed)
    {
        StopCoroutine ("GoTo");
        rigid.velocity = Vector3.zero;
        Vector3 targetPos = new Vector3 (target.position.x, target.position.y, transform.position.z);
        transform.position = Vector3.Lerp (transform.position, targetPos, Time.deltaTime * speed);
    }

    public IEnumerator GoTo (Vector3 target, float speed)
    {
        StopCoroutine ("FollowTarget");
        rigid.isKinematic = true;
        isEnabled = false;
        Vector3 camPos = transform.position;
        while (Vector2.Distance (transform.position, target) > 1)
        {
            camPos = Vector3.Lerp (transform.position, target, Time.deltaTime * speed);
            camPos.z = distance;
            transform.position = camPos;
            yield return null;
        }
        rigid.isKinematic = false;
        //isEnabled = true;
    }

    public IEnumerator SetMaxSize (float speed)
    {
        while (Camera.main.orthographicSize < maxCameraSize)
        {
            Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, maxCameraSize, Time.deltaTime * speed);
            yield return null;
        }
    }

    private void MoveWithMouse ()
    {
        Vector2 delta = Input.mousePosition - MyInput.lastMousePos;
            
        rigid.velocity = -delta * mouseMultiplier;

        // limit camera speed
        if (rigid.velocity.magnitude > maxCameraSpeed)
        {
            rigid.velocity = rigid.velocity.normalized * maxCameraSpeed;
        }

        //MyInput.lastMousePos = Input.mousePosition;
    }

    private void MoveWithKeyboard ()
    {
        if (Input.GetKey (KeyCode.S))
        {
            rigid.AddForce (Vector3.down * force);
        }
        else if (Input.GetKey (KeyCode.W))
        {
            rigid.AddForce (Vector3.up * force);
        }

        if (Input.GetKey (KeyCode.A))
        {
            rigid.AddForce (Vector3.left * force);
        }
        else if (Input.GetKey (KeyCode.D))
        {
            rigid.AddForce (Vector3.right * force);
        }

        // limit camera speed
        if (rigid.velocity.magnitude > maxCameraSpeed)
        {
            rigid.velocity = rigid.velocity.normalized * maxCameraSpeed;
        }
    }

    private void ZoomWithMouseWheel ()
    {
        Camera.main.orthographicSize -= Input.mouseScrollDelta.y * scrollZoomMultiplier;

        if (Camera.main.orthographicSize < minCameraSize)
        {
            Camera.main.orthographicSize = minCameraSize;
        }
        else if (Camera.main.orthographicSize > maxCameraSize)
        {
            Camera.main.orthographicSize = maxCameraSize;
        }
    }

    private void TouchMovement ()
    {
        // touch movement
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch (0);
            if (touch.phase == TouchPhase.Moved)
            {
                rigid.velocity = -touch.deltaPosition * touchMoveMultiplier;

                // limit camera speed
                if (rigid.velocity.magnitude > maxCameraSpeed)
                {
                    rigid.velocity = rigid.velocity.normalized * maxCameraSpeed;
                }
            }
        }
    }

    private void TouchZoom ()
    {
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch (0);
            Touch touchOne = Input.GetTouch (1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // ... change the orthographic size based on the change in distance between the touches.
            Camera.main.orthographicSize += deltaMagnitudeDiff * pinchZoomMultiplier;

            if (Camera.main.orthographicSize < minCameraSize)
            {
                Camera.main.orthographicSize = minCameraSize;
            }
            else if (Camera.main.orthographicSize > maxCameraSize)
            {
                Camera.main.orthographicSize = maxCameraSize;
            }
        }
    }
}