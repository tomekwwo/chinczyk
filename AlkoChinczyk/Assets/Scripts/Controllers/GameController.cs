﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameController : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject messagePanelPrefab;
    public GameObject confirmPanelPrefab;
    public Button diceButton;
    public Animator buttonAnimator;
    public Dice dice;
    public Player[] players;
    public Ranking ranking;
    public int maxPlayersInRow = 3;
    public Popup popupMessage;

    public static GameController instance;
    
    private int activePlayer = 0;
    private Text messageText;
    private Text diceButtonText;
    private CameraController cam;
    private GameData gameData;
    private int fieldsCount = 0;
    private int playersFinished = 0;
    private int winnerId = 0;

    void Awake ()
    {
        if (instance == null)
            instance = this;
        else
            Destroy (gameObject);
        
        ranking = GameObject.FindWithTag ("Ranking").GetComponent<Ranking> ();
    }

    void Start ()
    {
        fieldsCount = CountFields ();
        SpawnPlayers ();

        for (int i = 0; i < players.Length; i++)
        {
            ArrangePlayersOnField (players[i].field.number);
        }
        
        diceButtonText = diceButton.GetComponentInChildren<Text> ();
        SetDiceText (players[activePlayer].playerName + " " + Lang.instance.getString ("Throws"));
        diceButton.interactable = true;
        buttonAnimator.SetBool ("Pulse", true);
        cam = Camera.main.GetComponent<CameraController> ();
        cam.EnableControls (true);
        popupMessage.gameObject.SetActive (false);
    }

    int CountFields ()
    {
        return FindObjectsOfType<Field> ().Length;
    }

    public int GetFieldsCount ()
    {
        return fieldsCount;
    }

    void SpawnPlayers ()
    {
        gameData = GameSettings.instance.gameData;
        Destroy (GameSettings.instance.gameObject);
        Field startField = GameObject.FindWithTag ("StartField").GetComponent<Field> ();

        if (gameData != null)
        {
            players = new Player[gameData.playerName.Length];
            for (int i = 0; i < gameData.playerName.Length; i++)
            {
                // set players' settings
                GameObject playerObject = (GameObject) Instantiate (playerPrefab, startField.transform.position, startField.transform.rotation);
                players[i] = playerObject.GetComponent<Player> ();

                // set player name
                players[i].playerName = gameData.playerName[i];
                players[i].name = players[i].playerName;

                // set player color
                Color color = new Color (gameData.red[i], gameData.green[i], gameData.blue[i], gameData.alpha[i]);
                players[i].Color = color;

                // set player picture
                if (gameData.photo[i] == null)
                {
                    players[i].photo = players[i].blankPhoto;
                    players[i].photo.name = "Blank";
                }
                else
                {
                    Texture2D tex = new Texture2D (512, 512);
                    tex.LoadImage (gameData.photo[i]);
                    Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
                    players[i].photo = sprite;
                    players[i].photo.name = "Photo";
                }
                players[i].transform.Find ("Photo").GetComponent<SpriteRenderer> ().sprite = players[i].photo;

                // get player field
                if (gameData.fieldNR[i] == 0)
                {
                    players[i].field = startField;
                    players[i].transform.SetParent (startField.transform);
                    players[i].Spawn (players[i].field);
                }
                else
                {
                    if (gameData.fieldNR[i] >= fieldsCount)  // if player finished game
                    {
                        players[i].finished = true;
                        playersFinished++;
                        winnerId = gameData.winnerId;
                        players[i].Hide ();
                        players[i].gameObject.AddComponent<GhostField> ();

                        Field playerField = players[i].GetComponent<Field> ();

                        if (playerField)
                        {
                            playerField.number = gameData.fieldNR[i];
                            players[i].field = playerField;
                        }
                        else
                            Debug.Log ("Field null");
                    }
                    else
                    {
                        Field playerField = GameObject.Find ("Field (" + gameData.fieldNR[i] + ")").GetComponent<Field> ();

                        if (playerField)
                        {
                            players[i].field = playerField;
                            players[i].transform.SetParent (playerField.transform);
                            players[i].Spawn (players[i].field);
                        }
                    }
                }
            }
            activePlayer = gameData.activePlayer;
        }
        else
        {
            Debug.Log ("Can't get game data!");
        }
        
        Overseer.Save (players, activePlayer, winnerId, gameData.gameMode);
    }

    public void ArrangePlayersOnField (int fieldNumber)
    {
        // get all players standing on the field
        List<Player> playersOnField = new List<Player> ();
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].field.number == fieldNumber)
            {
                if (players[i].transform.parent != null)
                    playersOnField.Add (players[i]);
            }
        }

        // count rows
        int rows = 0;
        if (playersOnField.Count % maxPlayersInRow > 0)
            rows = (playersOnField.Count / maxPlayersInRow) + 1;
        else
            rows = playersOnField.Count / maxPlayersInRow;
        
        // count columns in each row
        int rowCount = rows;
        float columns = (float) playersOnField.Count / rowCount;

        // calculate distances
        float heightDist = Field.size / rows;
        Vector3 newPos = new Vector3 (0, (Field.size / 2) - (heightDist / 2), 0);

        // calculate scale
        Vector3 newScale = new Vector3 ();
        if (rows < columns)
            newScale = (Player.scale * 10) / columns;
        else
            newScale = (Player.scale * 10) / rows;
        newScale.z = 1;

        // arrange players
        for (int i = 0; i < rows; i++)
        {
            columns = (float) playersOnField.Count / rowCount;
            rowCount--;
            columns = Mathf.Ceil (columns);

            Player[] p = playersOnField.ToArray ();
            float dist = Field.size / columns;
            float margin = dist / 2;

            // first position
            newPos.x = -(Field.size / 2) + margin;
            float xPos = newPos.x;
            
            StartCoroutine (p[0].PositionAndScale (newPos, newScale));

            for (int j = 1; j < columns; j++)
            {
                xPos += dist;
                Vector3 nextPos = new Vector3 (xPos, newPos.y, 0);
                StartCoroutine (p[j].PositionAndScale (nextPos, newScale));
                playersOnField.RemoveAt (0);
            }
            playersOnField.RemoveAt (0);
            newPos.y -= heightDist;
        }
    }

    public void NextPlayer ()
    {
        if (activePlayer == players.Length - 1)
        {
            activePlayer = 0;
        }
        else
            activePlayer++;

        if (players[activePlayer].finished)
            NextPlayer ();

        diceButton.interactable = true;
        buttonAnimator.SetBool ("Pulse", true);
        SetDiceText (players[activePlayer].playerName + " " + Lang.instance.getString ("Throws"));
        Overseer.Save (players, activePlayer, winnerId, gameData.gameMode);
        cam.EnableControls (true);
    }

    public void Roll ()
    {
        buttonAnimator.SetBool ("Pulse", false);
        popupMessage.gameObject.SetActive (true);
        StartCoroutine (RollDice ());
    }

    public IEnumerator RollDice ()
    {
        yield return new WaitWhile (dice.isMoving);

        cam.EnableControls (false);
        StartCoroutine (cam.SetMaxSize (5));
        dice.enabled = true;
    }

    public void MoveActivePlayer (int fields)
    {
        diceButtonText.text = Lang.instance.getString ("YouThrew") + " " + fields.ToString ();
        StartCoroutine (players[activePlayer].MoveForward (fields));
    }

    public void GameOver (Player winner)
    {
        // game mode - to first one on the finish line
        if (gameData.gameMode == 0)
        {
            winnerId = activePlayer;
            FinishGame ();
        }
        // game mode - to last one on the finish line
        else if (gameData.gameMode == 1)
        {
            if (playersFinished == 0)
                winnerId = activePlayer;

            winner.finished = true;

            // create new field to set individual field number
            winner.gameObject.AddComponent<GhostField> ();
            winner.field = winner.GetComponent<GhostField> ();
            winner.field.number = fieldsCount + (players.Length - playersFinished);

            // make player invisible after reaching finish
            winner.Hide ();
            
            ranking.Refresh ();
            playersFinished++;

            if (playersFinished < players.Length)
                Overseer.instance.ShowPopup (winner.playerName + Lang.instance.getString ("FinishedOnPosition") + playersFinished);

            if (playersFinished < players.Length)
                NextPlayer ();
            else
            {
                FinishGame ();
            }
        }
        else    // unknown game mode
            Debug.Log ("Game mode nr " + gameData.gameMode + " not handled!");
    }

    private void FinishGame ()
    {
        if (File.Exists (Overseer.saveDirectory))
            File.Delete (Overseer.saveDirectory);

        ranking.gameObject.SetActive (true);
        Overseer.instance.GameOverAnimation ();
        Overseer.instance.ShowRateAppMessage ();
        Overseer.instance.ShowMessage (players[winnerId].playerName + " " + Lang.instance.getString ("Wins"), players[winnerId], LoadGame.Menu, Lang.instance.getString ("BackToMenu"));
    }

    void SetDiceText (string text)
    {
        Color color = players[activePlayer].Color;

        if ((color.r + color.g + color.b) >= 1.5f)
            color = Color.black;
        else
            color = Color.white;

        diceButtonText.color = color;
        diceButton.GetComponentInChildren<Image> ().color = players[activePlayer].Color;
        diceButtonText.text = text;
    }
}
