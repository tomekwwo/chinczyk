﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour
{
    private const bool ADS_ENABLED = false;
    private const bool ADS_TEST_MODE = false;
    private const string UNITY_ADS_GAME_ID = "1152873";

    public void Awake()
    {
        if (!Advertisement.isInitialized && ADS_ENABLED)
        {
            Advertisement.Initialize(UNITY_ADS_GAME_ID, ADS_TEST_MODE);
        }
    }

	public void ShowAd ()
    {
        if (!ADS_ENABLED)
        {
            return;
        }

        if (!Advertisement.isInitialized)
        {
            Debug.LogError("Cannot show ad; UnityAds is not initialized!");
            return;
        }

        Advertisement.Show();
    }
}
