﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.IO;

public class GameSettings : MonoBehaviour
{
    public static GameSettings instance;
    public GameObject playerSettingsPrefab;
    public GameObject scrollPanelContent;
    public GameObject popupPrefab;
    public GameObject confirmPanelPrefab;
    public Button continueButton;
    public GameObject loadingScreen;

    [HideInInspector]
    public Player[] players;
    [HideInInspector]
    public GameData gameData;

    private string selectedMap = "Standard";
    private int gameMode = 0;

    void Awake ()
    {
        DontDestroyOnLoad (gameObject);
        if (instance != null)
            Destroy (gameObject);
        else
            instance = this;

        if (File.Exists (Application.persistentDataPath + "/GameData.dat"))
        {
            continueButton.interactable = true;
        }
        else
            continueButton.interactable = false;
    }

    public void AddPlayer ()
    {
        continueButton.interactable = false;
        Instantiate (playerSettingsPrefab, scrollPanelContent.transform);
    }

    public void RemovePlayer ()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag ("PlayerSettings");
        if (File.Exists (Application.persistentDataPath + "/GameData.dat") && players.Length <= 1)
        {
            continueButton.interactable = true;
        }
    }

    public void SetMapName (string map)
    {
        selectedMap = map;
    }

    public void SetGameMode (int mode)
    {
        gameMode = mode;
    }

    public void StartGame ()
    {
        GameObject[] playerSettings = GameObject.FindGameObjectsWithTag ("PlayerSettings");

        // check if there is at least 1 player
        if (playerSettings.Length == 0)
        {
            Overseer.instance.ShowPopup (Lang.instance.getString ("NeedMorePlayers"));
            return;
        }

        players = new Player[playerSettings.Length];
        for (int i = 0; i < players.Length; i++)
        {
            players[i] = playerSettings[i].GetComponent<Player> ();
        }

        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].playerName == "")    // check if player has a name
            {
                Overseer.instance.ShowPopup (Lang.instance.getString ("EveryoneNeedsName"));
                return;
            }
            else
            {
                // check if player name starts with a letter
                //if (players[i].playerName[0] < 33 || players[i].playerName[0] > 126)
                if (players[i].playerName[0] == ' ')
                {
                    Overseer.instance.ShowPopup (Lang.instance.getString ("WrongName"));
                    Debug.Log ("First letter: " + players[i].playerName[0] + " (int: " + (int) players[i].playerName[0] + ")");
                    return;
                }

                // count players with given name
                int counter = 0;
                foreach (Player player in players)
                {
                    if (players[i].playerName == player.playerName)
                        counter++;
                }

                // check if names repeat
                if (counter > 1)
                {
                    Overseer.instance.ShowPopup (Lang.instance.getString ("NamesRepeat"));
                    return;
                }
            }
        }

        // convert players data to GameData
        gameData = new GameData (players.Length);
        for (int i = 0; i < players.Length; i++)
        {
            gameData.playerName[i] = players[i].playerName;
            gameData.red[i] = players[i].Color.r;
            gameData.green[i] = players[i].Color.g;
            gameData.blue[i] = players[i].Color.b;
            gameData.alpha[i] = players[i].Color.a;

            if (players[i].photo == players[i].blankPhoto)
                gameData.photo[i] = null;
            else
                gameData.photo[i] = players[i].photo.texture.EncodeToPNG ();

            gameData.fieldNR[i] = 0;
        }
        gameData.activePlayer = 0;
        gameData.map = selectedMap;
        gameData.gameMode = gameMode;

        if (File.Exists (Overseer.saveDirectory))
        {
            UnityAction action = () => { File.Delete (Overseer.saveDirectory); ShowLoadingScreen (); LoadGame.LoadScene (selectedMap, true); };
            Overseer.instance.ShowConfirmPanel (Lang.instance.getString ("SaveOverwrite"), action, null);
        }
        else
        {
            ShowLoadingScreen ();

            LoadGame.LoadScene (selectedMap, true);
        }
    }

    public void Continue ()
    {
        ShowLoadingScreen ();
        gameData = Overseer.Load ();

        if (gameData != null)
        {
            LoadGame.LoadScene (gameData.map, true);
        }
    }

    void ShowLoadingScreen ()
    {
        GameObject screen = Instantiate (loadingScreen);
        screen.transform.SetParent (GameObject.FindWithTag ("MainCanvas").GetComponent<Transform> ());
        RectTransform rect = screen.GetComponent<RectTransform> ();
        rect.anchoredPosition = new Vector2 (0, 0);
        rect.sizeDelta = new Vector2 (0, 0);
        rect.localScale = new Vector3 (1, 1, 1);
    }
}
