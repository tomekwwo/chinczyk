﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class GameData
{
    public string[] playerName;
    public float[] red;
    public float[] green;
    public float[] blue;
    public float[] alpha;
    public byte[][] photo;
    public int[] fieldNR;
    public int activePlayer;
    public int winnerId;
    public string map;
    public int gameMode;

    public GameData (int players)
    {
        playerName = new string[players];

        // color
        red = new float[players];
        green = new float[players];
        blue = new float[players];
        alpha = new float[players];

        photo = new byte[players][];
        fieldNR = new int[players];
        activePlayer = 0;
        winnerId = 0;
        map = "Standard";
        gameMode = 0;
    }
}

public class Overseer : MonoBehaviour
{
    public const string GAME_RATED_KEY = "game_rated";
    public const string AUDIO_MUTED_KEY = "audio_muted";

    public static Overseer instance;
    public static GameObject mainCanvas;
    public static string saveDirectory;
    public static string optionsDirectory;
    //public static float timeWithoutAd = 0;
    //public static float adsInterval = 1800;

    public UnityAds Ads { get; private set; }

    private GameObject dialogs;
    private MessagePanel messagePanel;
    private MessagePanel confirmPanel;
    private MessagePanel quitPanel;
    private Popup popup;
    private PickerController colorPicker;

    private Dictionary<string, bool> boolDict = new Dictionary<string, bool>();

    void Awake ()
    {
        DontDestroyOnLoad (gameObject);
        if (instance != null)
        {
            Destroy (gameObject);
        }   
        else
        {
            instance = this;
        }
        if (this != instance) return;

        Ads = GetComponent<UnityAds>();
        saveDirectory = Application.persistentDataPath + "/GameData.dat";
        optionsDirectory = Application.persistentDataPath + "/Options.dat";
        LoadOptions ();
        InitUI ();
        
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (instance != this) return;

        InitUI ();
    }

    void Update ()
    {
        if (Input.GetKeyDown (KeyCode.Escape))
        {
            ShowQuitMessage ();
        }
    }

    void InitUI ()
    {
        mainCanvas = GameObject.FindWithTag ("MainCanvas");

        dialogs = mainCanvas.transform.Find ("Dialogs").gameObject;
        if (dialogs)
        {
            quitPanel = dialogs.transform.GetChild (0).GetComponent<MessagePanel> ();
            messagePanel = dialogs.transform.GetChild (1).GetComponent<MessagePanel> ();
            confirmPanel = dialogs.transform.GetChild (2).GetComponent<MessagePanel> ();
            popup = dialogs.transform.GetChild (3).GetComponent<Popup> ();
            if (dialogs.transform.childCount > 4)
            {
                colorPicker = dialogs.transform.GetChild(4).GetComponent<PickerController>();
            }
        }
        else
            Debug.LogWarning ("Overseer couldn't find dialogs!");
    }

    public void ShowPopup (string message, float time = 2)
    {
        popup.gameObject.SetActive (false);
        popup.showTime = time;
        popup.gameObject.SetActive (true);
        popup.message.text = message;
    }

    public void ShowMessage (string message)
    {
        messagePanel.gameObject.SetActive (true);
        messagePanel.message.text = message;
    }

    public void ShowMessage (string msg, Player player, UnityAction action, string buttonText)
    {
        messagePanel.gameObject.SetActive (true);
        messagePanel.ShowMessage (msg, player.playerName, player.Color, player.photo, buttonText);
        messagePanel.AddAction (action);
    }

    public void ShowTranslatedMessage (string translationPhrase)
    {
        ShowMessage (Lang.instance.getString (translationPhrase));
    }

    public void ShowConfirmPanel (string msg, UnityAction confirmAction, UnityAction declineAction)
    {
        confirmPanel.gameObject.SetActive (true);
        confirmPanel.message.text = msg;
        confirmPanel.AddAction (confirmAction);

        if (declineAction != null)
            confirmPanel.declineButton.onClick.AddListener (declineAction);

        confirmPanel.declineButton.onClick.AddListener (() => { confirmPanel.gameObject.SetActive (false); });
    }

    public void ShowQuitMessage ()
    {
        quitPanel.gameObject.SetActive (true);
        
        if (LoadGame.GetActiveScene () == 0)
        {
            quitPanel.message.text = Lang.instance.getString ("QuitGame");
            quitPanel.AddAction (Application.Quit);
        }
        else
        {
            quitPanel.message.text = Lang.instance.getString ("BackToMenu") + "?";
            quitPanel.AddAction (LoadGame.Customize);
        }

        quitPanel.declineButton.onClick.AddListener (() => { quitPanel.gameObject.SetActive (false); });
    }

    public void ShowRateAppMessage ()
    {   
        if (!GetOption(GAME_RATED_KEY))
        {
            ShowConfirmPanel (Lang.instance.getString ("RateApp"), () => { Application.OpenURL ("market://details?id=com.WidlyWGnoju.PijanyChinczyk"); confirmPanel.gameObject.SetActive (false); }, null);

            // don't show rate app message more than once
            SetOption(GAME_RATED_KEY, true);
        }
    }

    public void ShowColorPicker(Color currentColor, Image targetImage)
    {
        colorPicker.gameObject.SetActive(true);
        colorPicker.Init(currentColor, targetImage);
    }

    public void GameOverAnimation ()
    {
        StartCoroutine (InitGameOverAnimation ());
    }

    private IEnumerator InitGameOverAnimation ()
    {
        yield return new WaitForEndOfFrame ();
        mainCanvas.GetComponent<Animator> ().SetTrigger ("GameOver");
    }

    public static void Save (Player[] players, int activePlayer, int winner, int gameMode)
    {
        BinaryFormatter bf = new BinaryFormatter ();
        FileStream file = File.Open (saveDirectory, FileMode.OpenOrCreate);

        GameData data = new GameData (players.Length);

        for (int i = 0; i < players.Length; i++)
        {
            data.playerName[i] = players[i].playerName;

            data.red[i] = players[i].Color.r;
            data.green[i] = players[i].Color.g;
            data.blue[i] = players[i].Color.b;
            data.alpha[i] = players[i].Color.a;

            if (players[i].photo == players[i].blankPhoto)
                data.photo[i] = null;
            else
                data.photo[i] = players[i].photo.texture.EncodeToPNG ();

            data.fieldNR[i] = players[i].field.number;
        }
        data.activePlayer = activePlayer;
        data.winnerId = winner;
        data.map = LoadGame.GetActiveSceneName ();
        data.gameMode = gameMode;

        bf.Serialize (file, data);
        file.Close ();
    }

    public static GameData Load ()
    {
        if (File.Exists (saveDirectory))
        {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Open (saveDirectory, FileMode.Open);
            GameData gameData = (GameData) bf.Deserialize (file);
            file.Close ();

            return gameData;
        }
        else
            return null;
    }

    public static void SetOption(string key, bool value)
    {
        instance.boolDict[key] = value;
    }

    public static bool GetOption(string key, bool defaultValue = false)
    {
        if (instance.boolDict.ContainsKey(key))
        {
            return instance.boolDict[key];
        }

        return defaultValue;
    }

    public static void SaveOptions ()
    {
        BinaryFormatter bf = new BinaryFormatter ();
        FileStream file = File.Open (optionsDirectory, FileMode.OpenOrCreate);

        bf.Serialize (file, instance.boolDict);
        file.Close ();
    }

    public static void LoadOptions ()
    {
        if (File.Exists (optionsDirectory))
        {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Open (optionsDirectory, FileMode.Open);
            instance.boolDict = (Dictionary<string, bool>)bf.Deserialize(file);
            file.Close ();
        }
    }

    public static byte[] LoadFile (string path)
    {
        return File.ReadAllBytes (path);
    }
}
