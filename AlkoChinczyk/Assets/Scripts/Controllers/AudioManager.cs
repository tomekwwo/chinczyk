﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent (typeof (AudioSource))]
public class AudioManager : MonoBehaviour 
{
    public static AudioManager instance;

    [SerializeField] private Button muteButton;
    [SerializeField] private Button playButton;

    private AudioSource source;

    void Awake ()
    {
        if (instance == null)
            instance = this;
        else
            Destroy (gameObject);
        if (instance != this) return;

        source = GetComponent<AudioSource> ();
        
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (instance == this)
        {
            if (Overseer.GetOption(Overseer.AUDIO_MUTED_KEY))
            {
                source.Stop();
            }
            else if (!source.isPlaying)
            {
                source.Play();
            }
            FindAudioControls ();
        }
    }

    void Start ()
    {
        FindAudioControls ();
    }

    void FindAudioControls ()
    {
        muteButton = GameObject.FindWithTag ("MuteButton").GetComponent<Button> ();
        var tmp = GameObject.FindWithTag("PlayButton");
        if (tmp)
        {
            playButton = tmp.GetComponent<Button> ();   
        }

        if (muteButton && playButton)
        {
            muteButton.onClick.AddListener (StopMusic);
            playButton.onClick.AddListener (PlayMusic);

            if (source.isPlaying)
            {
                playButton.gameObject.SetActive (false);
            }
        }
    }

    private void PlayMusic()
    {
        if (!source.isPlaying)
        {
            source.Play();
        }

        Overseer.SetOption(Overseer.AUDIO_MUTED_KEY, false);
    }

    private void StopMusic()
    {
        source.Stop();
        Overseer.SetOption(Overseer.AUDIO_MUTED_KEY, true);
    }
}