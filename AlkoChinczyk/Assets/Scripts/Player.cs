﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float moveSpeed = 10;
    public Sprite photo;
    public Sprite blankPhoto;
    public bool finished = false;

    public static Vector3 scale = new Vector3 (3, 3, 1);

    public Field field;

    public string playerName { get; set; }

    public Color Color
    {
        get
        {
            if (colorImage != null)
            {
                return colorImage.color;   
            }
            else
            {
                if (spriteRenderer == null)
                {
                    spriteRenderer = transform.Find("Base").GetComponent<SpriteRenderer>();
                }

                return spriteRenderer.color;
            }
        }
        set
        {
            if (colorImage != null)
            {
                colorImage.color = value;
            }
            else
            {
                if (spriteRenderer == null)
                {
                    spriteRenderer = transform.Find("Base").GetComponent<SpriteRenderer>();
                }

                spriteRenderer.color = value;
            }
        }
    }

    [SerializeField] private Image colorImage;
    
    private bool beingMoved = false;
    private SpriteRenderer spriteRenderer;

    void Awake ()
    {
        playerName = "";
        Color = new Color (Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), 1.0f);
    }

    //void Update ()
    //{
    //    if (finished)
    //        Debug.Log (playerName + " on nr " + field.number);
    //}

    public void Spawn (Field spawnField)
    {
        field = spawnField;
        transform.position = spawnField.transform.position;
        transform.rotation = spawnField.transform.rotation;
    }

    public void Hide ()
    {
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer> ();
        foreach (SpriteRenderer renderer in renderers)
        {
            renderer.enabled = false;
        }
    }

    public IEnumerator PositionAndScale (Vector3 position, Vector3 scale)
    {
        yield return new WaitWhile (isMoving);

        beingMoved = true;
        while (Vector3.Distance (transform.localPosition, position) > 0.5f || Vector3.Distance (transform.localScale, scale) > 0.5f)
        {
            transform.localPosition = Vector3.Lerp (transform.localPosition, position, Time.deltaTime * moveSpeed);
            transform.localScale = Vector3.Lerp (transform.localScale, scale, Time.deltaTime * (moveSpeed / 2));

            yield return null;
        }
        transform.localPosition = position;
        transform.localScale = scale;
        beingMoved = false;
    }

    public IEnumerator MoveForward (int fields)
    {
        yield return new WaitWhile (isMoving);
        beingMoved = true;
        transform.SetParent (null);
        transform.position = field.transform.position;
        transform.localScale = scale;
        GameController.instance.ArrangePlayersOnField (field.number);

        Camera.main.GetComponent<CameraController> ().EnableControls (false);
        for (int i = 0; i < fields; i++)
        {
            while (Vector3.Distance (transform.position, field.nextField.transform.position) > 0.05f)
            {
                transform.position = Vector3.Lerp (transform.position, field.nextField.transform.position, Time.deltaTime * moveSpeed);
                Camera.main.GetComponent<CameraController> ().FollowTarget (transform, moveSpeed);

                yield return null;
            }
            transform.position = field.nextField.transform.position;
            field = field.nextField;
        }
        GameController.instance.ranking.Refresh ();
        transform.SetParent (field.transform);
        Vector3 newScale = scale * 10;
        newScale.z = 1;
        transform.localScale = newScale;
        beingMoved = false;
        GameController.instance.ArrangePlayersOnField (field.number);

        field.Activate (this);
    }

    public IEnumerator MoveBackward (int fields)
    {
        yield return new WaitWhile (isMoving);
        beingMoved = true;
        transform.SetParent (null);
        transform.localScale = scale;
        GameController.instance.ArrangePlayersOnField (field.number);

        Camera.main.GetComponent<CameraController> ().EnableControls (false);
        for (int i = 0; i < fields; i++)
        {
            while (Vector3.Distance (transform.position, field.previousField.transform.position) > 0.05f)
            {
                transform.position = Vector3.Lerp (transform.position, field.previousField.transform.position, Time.deltaTime * moveSpeed);
                Camera.main.GetComponent<CameraController> ().FollowTarget (transform, moveSpeed);

                yield return null;
            }
            transform.position = field.previousField.transform.position;
            field = field.previousField;
        }
        GameController.instance.ranking.Refresh ();
        transform.SetParent (field.transform);
        Vector3 newScale = scale * 10;
        newScale.z = 1;
        transform.localScale = newScale;
        beingMoved = false;
        GameController.instance.ArrangePlayersOnField (field.number);

        Camera.main.GetComponent<CameraController> ().EnableControls (true);

        field.Activate (this);
    }

    public void MoveTo (int fieldNR)
    {
        int fields;
        if (fieldNR > field.number)
        {
            fields = Mathf.Abs (fieldNR - field.number);
            StartCoroutine (MoveForward (fields));
        }
        else
        {
            fields = Mathf.Abs (fieldNR - field.number);
            StartCoroutine (MoveBackward (fields));
        }
    }

    public bool isMoving ()
    {
        return beingMoved;
    }

    public void PickColor()
    {
        Overseer.instance.ShowColorPicker(Color, colorImage);
    }
}
