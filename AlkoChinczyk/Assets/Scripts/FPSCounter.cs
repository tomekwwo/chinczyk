﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof (Text))]
public class FPSCounter : MonoBehaviour {

    public float frequency = 1f;

    Text fpsText;
    int frames;
    float timer, fps;
    int lowest, highest;

    void Start()
    {
        fpsText = GetComponent<Text>();
        fpsText.text = "";
        frames = 0;
        timer = 0f;
        lowest = 60;
        highest = 0;
    }

	void Update () 
    {
        timer += Time.deltaTime;

        if(timer >= frequency)
        {
            fps = Mathf.Round((frames / timer));
            
            frames = 0;
            timer = 0f;
            if(Time.time > 3f)
            {
                if (fps < lowest)
                    lowest = (int)fps;
                if (fps > highest)
                    highest = (int)fps;

                fpsText.text = "FPS: " + fps.ToString() + " L: " + lowest + " H: " + highest + " A: " + ((lowest + highest) / 2).ToString();
            }
            else
                fpsText.text = "FPS: " + fps.ToString();
        }
        frames++;
	}
}
