﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageBox : MonoBehaviour
{
    public GameObject messageBoxPrefab;

    private GameObject messageBox;
    private Text messageText;

    private void InstantiateMessageBox ()
    {
        messageBox = Instantiate (messageBoxPrefab);

        messageBox.transform.SetParent (Overseer.mainCanvas.transform);
        RectTransform rect = messageBox.GetComponent<RectTransform> ();
        rect.anchoredPosition = Vector2.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = Vector3.one;
        messageText = messageBox.transform.GetChild (0).GetComponent<Text> ();
    }

    public void ShowMessage (string msg)
    {
        InstantiateMessageBox ();
        
        messageText.text = msg;
    }

    public void ShowTranslatedMessage (string translationPhrase)
    {
        InstantiateMessageBox ();

        messageText.text = Lang.instance.getString (translationPhrase);
    }
}
 