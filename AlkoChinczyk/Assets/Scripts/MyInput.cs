﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyInput : MonoBehaviour 
{
    public static bool UIhit = false;
    public static bool cursorHolding = false;
    public static Vector3 lastMousePos;

    void Update ()
    {
        if (Input.touchSupported)
        {
            TouchInput ();
        }
        else
        {
            MouseInput ();
        }
    }

    void MouseInput ()
    {
        if (Input.GetMouseButtonDown (0))
        {
            cursorHolding = true;

            var system = EventSystem.current;
            var hits = new List<RaycastResult> ();
            var pointer = new PointerEventData (system);
            pointer.position = Input.mousePosition;
            system.RaycastAll (pointer, hits);

            foreach (RaycastResult result in hits)
            {
                if (result.gameObject.layer == 5)
                {
                    UIhit = true;
                    break;
                }
            }

            lastMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButton (0))
        {
            lastMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp (0))
        {
            UIhit = false;
            cursorHolding = false;
            lastMousePos = Input.mousePosition;
        }
    }

    void TouchInput ()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch (0);

            if (touch.phase == TouchPhase.Began)
            {
                cursorHolding = true;

                var system = EventSystem.current;
                var hits = new List<RaycastResult> ();
                var pointer = new PointerEventData (system);
                pointer.position = touch.position;
                system.RaycastAll (pointer, hits);

                foreach (RaycastResult result in hits)
                {
                    if (result.gameObject.layer == 5)
                    {
                        UIhit = true;
                        break;
                    }
                }

                lastMousePos = Input.mousePosition;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                UIhit = false;
                cursorHolding = false;
                lastMousePos = Input.mousePosition;
            }
        }
    }
}