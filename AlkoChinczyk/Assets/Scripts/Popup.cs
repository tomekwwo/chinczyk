﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    public float lerpSpeed = 5;
    public float showTime = 2;
    public bool destroy = true;
    [HideInInspector]
    public Text message;

    private Image image;
    private float timer = 0;
    private RectTransform rect;
    private GameObject canvas;

    void Awake ()
    {
        image = GetComponent<Image> ();
        message = GetComponentInChildren<Text> ();
        rect = GetComponent<RectTransform> ();

        image.color = new Color (image.color.r, image.color.g, image.color.b, 0);
        message.color = new Color (message.color.r, message.color.g, message.color.b, 0);

        canvas = GameObject.Find ("Canvas");
    }
    
    public void ShowMessage ()
    {
        StartCoroutine (Show (destroy));
    }

    void OnEnable ()
    {
        ShowMessage ();
    }

    IEnumerator Show (bool destroy)
    {
        while (image.color.a < 0.95f && message.color.a < 0.95f)
        {
            image.color = Color.Lerp (image.color, new Color (image.color.r, image.color.g, image.color.b, 1), Time.deltaTime * lerpSpeed);
            message.color = Color.Lerp (message.color, new Color (message.color.r, message.color.g, message.color.b, 1), Time.deltaTime * lerpSpeed);
            yield return null;
        }
        image.color = new Color (image.color.r, image.color.g, image.color.b, 1);
        message.color = new Color (message.color.r, message.color.g, message.color.b, 1);

        timer = 0;

        while (timer < showTime)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        StartCoroutine (Hide (destroy));
    }

    IEnumerator Hide (bool destroy)
    {
        while (image.color.a >= 0.05f && message.color.a >= 0.05f)
        {
            image.color = Color.Lerp (image.color, new Color (image.color.r, image.color.g, image.color.b, 0), Time.deltaTime * lerpSpeed);
            message.color = Color.Lerp (message.color, new Color (message.color.r, message.color.g, message.color.b, 0), Time.deltaTime * lerpSpeed);
            yield return null;
        }
        image.color = new Color (image.color.r, image.color.g, image.color.b, 0);
        message.color = new Color (message.color.r, message.color.g, message.color.b, 0);

        if (destroy)
            Destroy (gameObject);
        else
            gameObject.SetActive (false);
    }

    public void ShowPopup (string msg)
    {
        message.text = msg;
        transform.SetParent (canvas.transform);
        rect.anchoredPosition = Vector2.zero;
        rect.sizeDelta = Vector2.zero;
        rect.localScale = new Vector3 (1, 1, 1);
        StartCoroutine (Show (destroy));
    }
}
