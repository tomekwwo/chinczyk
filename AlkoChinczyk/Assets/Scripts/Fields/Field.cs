﻿using UnityEngine;
using UnityEngine.UI;

public class Field : MonoBehaviour
{
    public int number;
    public Field nextField;
    public Field previousField;
    public Text fieldText;
    public string translationPhrase;

    public static float size = 200;

    void Start ()
    {
        Initialize ();
    }

    public virtual void Initialize ()
    {
        // set field number
        if (tag == "StartField")
        {
            number = 0;
        }
        else
        {
            // get field number from game object name
            bool read = false;
            string nr = "";
            foreach (char c in name)
            {
                if (c == ')')
                    break;

                if (read)
                    nr += c;

                if (c == '(')
                    read = true;
            }

            int.TryParse (nr, out number);
        }

        // find next field
        if (!nextField)
        {
            GameObject field = GameObject.Find ("Field (" + (number + 1) + ")");

            if (field)
            {
                nextField = field.GetComponent<Field> ();
            }
            else
            {
                nextField = GetComponent<Field> ();
            }
        }

        // find previous field
        if (!previousField)
        {
            if (number == 1)
            {
                previousField = GameObject.FindWithTag ("StartField").GetComponent<Field> ();
            }
            else
            {
                GameObject field = GameObject.Find ("Field (" + (number - 1) + ")");

                if (field)
                {
                    previousField = field.GetComponent<Field> ();
                }
                else
                {
                    previousField = GetComponent<Field> ();
                }
            }
        }

        // set field text
        if (!fieldText)
            fieldText = GetComponentInChildren<Text> ();
        
        if (fieldText && fieldText.text == "")
        {
            fieldText.text = number.ToString ();
        }

        SetText ();
    }

    public void SetText ()
    {
        if (fieldText && translationPhrase != "")
        {
            fieldText.text = Lang.instance.getString (translationPhrase);
        }
    }

    public virtual void Activate (Player player)
    {
        if (nextField == this)
        {
            GameController.instance.GameOver (player);
            return;
        }
        
        if (!IsNumber (fieldText.text) && tag != "StartField")
        {
            Overseer.instance.ShowMessage (fieldText.text, player, GameController.instance.NextPlayer, Lang.instance.getString ("Done"));
        }
        else
            GameController.instance.NextPlayer ();
    }

    bool IsNumber (string text)
    {
        int tmp;
        if (int.TryParse (text, out tmp))
            return true;
        else
            return false;
    }
}
