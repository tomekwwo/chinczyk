﻿public class MoveToField : Field 
{
    public int moveTo;

    private Player player;

    void Start ()
    {
        base.Initialize ();
    }

    public override void Activate (Player player)
    {
        this.player = player;
        Overseer.instance.ShowMessage (fieldText.text, player, Move, Lang.instance.getString ("LetsGo"));
    }

    void Move ()
    {
        player.MoveTo (moveTo);
    }
}