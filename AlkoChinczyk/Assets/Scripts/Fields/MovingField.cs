﻿using UnityEngine;

public class MovingField : Field
{
    public int fieldsToMove;
    public bool forward = true;

    private Player player;

    void Start ()
    {
        base.Initialize ();

        if (fieldsToMove < 0)
            fieldsToMove = Mathf.Abs (fieldsToMove);
    }

    public override void Activate (Player player)
    {
        this.player = player;
        Overseer.instance.ShowMessage (fieldText.text, player, Move, Lang.instance.getString ("LetsGo"));
    }

    void Move ()
    {
        if (forward)
            StartCoroutine (player.MoveForward (fieldsToMove));
        else
            StartCoroutine (player.MoveBackward (fieldsToMove));
    }
}